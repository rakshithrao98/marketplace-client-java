package com.atlassian.marketplace.client.model;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation is used internally to denote model properties that should not be serialized.
 * @since 2.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ReadOnly
{
}
