package com.atlassian.marketplace.client.model;

import org.joda.time.LocalDate;

import java.net.URI;

/**
 * Information about a specific version of an {@link Application}.
 * @see com.atlassian.marketplace.client.api.Applications
 */
public final class ApplicationVersion implements Entity
{
    Links _links;
    @RequiredLink(rel = "self") URI selfUri;
    
    Integer buildNumber;
    String version;
    LocalDate releaseDate;
    ApplicationVersionStatus status;
    boolean dataCenterCompatible;
    
    @Override
    public Links getLinks()
    {
        return _links;
    }
    
    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }

    /**
     * The version's build number, a value specified by Atlassian that distinguishes it from all other
     * versions of the application and determines the correct ordering of versions.
     */
    public int getBuildNumber()
    {
        return buildNumber;
    }
    
    /**
     * The version string, e.g. "1.0".
     */
    public String getName()
    {
        // changing property name from "version" to "name" for consistency with AddonVersion
        return version;
    }

    /**
     * The date on which the version was released.
     */
    public LocalDate getReleaseDate()
    {
        return releaseDate;
    }
    
    /**
     * Indicates whether the version is publicly visible or private.
     */
    public ApplicationVersionStatus getStatus()
    {
        return status;
    }

    /**
     * Indicates whether the version is Data Center compatible or not
     */
    public boolean isDataCenterCompatible()
    {
        return dataCenterCompatible;
    }
}
