package com.atlassian.marketplace.client.model;

import com.atlassian.marketplace.client.api.VendorId;
import io.atlassian.fugue.Option;

import java.net.URI;

/**
 * Properties that exist in both {@link Vendor} and {@link VendorSummary}.
 * @since 2.0.0
 */
public abstract class VendorBase implements Entity
{
    Links _links;
    String name;
    @ReadOnly Option<String> verifiedStatus;
    @ReadOnly Option<Boolean> isAtlassian;
    VendorPrograms programs;
    @RequiredLink(rel = "self") URI selfUri;
    @RequiredLink(rel = "alternate") URI alternateUri;
    
    @Override
    public Links getLinks()
    {
        return _links;
    }

    /**
     * The unique resource identifier for the vendor.
     * @see AddonBase#getVendorId()
     * @see ModelBuilders.AddonBuilder#vendor(VendorId)
     * @see com.atlassian.marketplace.client.api.Vendors#safeGetById(VendorId)
     */
    public VendorId getId()
    {
        return VendorId.fromUri(selfUri);
    }

    @Override
    public URI getSelfUri()
    {
        return selfUri;
    }
    
    /**
     * The address of a Marketplace page that shows details of the vendor.
     */
    public URI getAlternateUri()
    {
        return alternateUri;
    }
    
    /**
     * The vendor name.
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * The vendor logo.
     */
    public abstract Option<ImageInfo> getLogo();

    /**
     * True if the vendor has Atlassian Verified status.
     */
    public boolean isVerified()
    {
        return "verified".equalsIgnoreCase(verifiedStatus.getOrElse(""));
    }

    /**
     * True if the vendor is one of Atlassian's
     */
    public boolean isAtlassian() {
        return isAtlassian.exists(Boolean.TRUE::equals);
    }

    /**
     * True if the vendor is enrolled in TopVendor program     *
     */

    public boolean isTopVendor(){
        return programs.topVendor.exists(tv -> TopVendorStatus.APPROVED == tv.status);
    }

    /**
     * Programs that the vendor is enrolled in.
     */
    public VendorPrograms getPrograms() {
        return programs;
    }
}
