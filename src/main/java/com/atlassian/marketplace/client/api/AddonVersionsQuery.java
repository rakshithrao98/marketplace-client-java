package com.atlassian.marketplace.client.api;

import java.util.Optional;

import static com.atlassian.marketplace.client.api.QueryProperties.describeOptEnum;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.atlassian.marketplace.client.api.QueryProperties.describeValues;
import static com.atlassian.marketplace.client.util.Convert.iterableOf;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Encapsulates search parameters that can be passed to {@link Addons#safeGetVersion}.
 * @since 2.0.0
 */
public final class AddonVersionsQuery implements QueryProperties.AccessToken,
        QueryProperties.ApplicationCriteria,
        QueryProperties.Bounds,
        QueryProperties.Cost,
        QueryProperties.Hosting,
        QueryProperties.IncludePrivate
{
    private static final AddonVersionsQuery DEFAULT_QUERY = builder().build();

    private final boolean includePrivate;
    private final Optional<String> accessToken;
    private final QueryBuilderProperties.ApplicationCriteriaHelper app;
    private final Optional<Cost> cost;
    private final Optional<HostingType> hosting;
    private final QueryBounds bounds;
    private final Optional<String> afterVersionName;
    
    /**
     * Returns a new {@link Builder} for constructing an AddonVersionsQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns an AddonVersionsQuery with no criteria, which will match any available versions.
     */
    public static AddonVersionsQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    public static Builder fromAddonQuery(AddonQuery aq)
    {
        return builder()
            .accessToken(aq.safeGetAccessToken())
            .application(aq.safeGetApplication())
            .appBuildNumber(aq.safeGetAppBuildNumber())
            .cost(aq.safeGetCost())
            .hosting(aq.safeGetHosting())
            .bounds(aq.getBounds());
    }
    
    /**
     * Returns a new {@link Builder} for constructing an AddonVersionsQuery based on an existing AddonVersionsQuery.
     */
    public static Builder builder(AddonVersionsQuery query)
    {
        Builder builder = builder()
            .application(query.safeGetApplication())
            .appBuildNumber(query.safeGetAppBuildNumber())
            .cost(query.safeGetCost())
            .hosting(query.safeGetHosting())
            .bounds(query.getBounds());

        return builder;
    }

    private AddonVersionsQuery(Builder builder)
    {
        accessToken = builder.accessToken;
        app = builder.app;
        cost = builder.cost;
        hosting = builder.hosting;
        bounds = builder.bounds;
        afterVersionName = builder.afterVersionName;
        includePrivate = builder.includePrivate;
    }

    @Override
    public Optional<String> safeGetAccessToken()
    {
        return accessToken;
    }

    @Override
    public Optional<ApplicationKey> safeGetApplication()
    {
        return app.application;
    }

    @Override
    public Optional<Integer> safeGetAppBuildNumber()
    {
        return app.appBuildNumber;
    }

    @Override
    public Optional<Cost> safeGetCost()
    {
        return cost;
    }

    @Override
    public Optional<HostingType> safeGetHosting()
    {
        return hosting;
    }

    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }
    
    /**
     * The version string, if any, that the client has specified in order to search for
     * later versions.
     * @see Builder#afterVersion(Optional)
     */
    public Optional<String> safeGetAfterVersionName()
    {
        return afterVersionName;
    }

    @Override
    public boolean isIncludePrivate()
    {
        return includePrivate;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("AddonVersionsQuery",
            describeValues("accessToken", iterableOf(accessToken)),
            app.describe(),
            describeOptEnum("cost", iterableOf(cost)),
            describeOptEnum("hosting", iterableOf(hosting)),
            bounds.describe(),
            describeValues("afterVersion", iterableOf(afterVersionName))
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof AddonVersionsQuery) && toString().equals(other.toString());
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    /**
     * Builder class for {@link AddonVersionsQuery}.  Use {@link AddonVersionsQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.AccessToken<Builder>,
        QueryBuilderProperties.ApplicationCriteria<Builder>,
        QueryBuilderProperties.Bounds<Builder>,
        QueryBuilderProperties.Cost<Builder>,
        QueryBuilderProperties.Hosting<Builder>,
        QueryBuilderProperties.IncludePrivate<Builder>
    {
        private boolean includePrivate;
        private Optional<String> accessToken = Optional.empty();
        private QueryBuilderProperties.ApplicationCriteriaHelper app = new QueryBuilderProperties.ApplicationCriteriaHelper();
        private Optional<Cost> cost = Optional.empty();
        private Optional<HostingType> hosting = Optional.empty();
        private QueryBounds bounds = QueryBounds.defaultBounds();
        private Optional<String> afterVersionName = Optional.empty();
        
        /**
         * Returns an immutable {@link AddonVersionsQuery} based on the current builder properties.
         */
        public AddonVersionsQuery build()
        {
            return new AddonVersionsQuery(this);
        }

        @Override
        public Builder accessToken(Optional<String> accessToken)
        {
            this.accessToken = checkNotNull(accessToken);
            return this;
        }

        @Override
        public Builder application(Optional<ApplicationKey> application){
            app = app.application(application);
            return this;
        }

        @Override
        public Builder appBuildNumber(Optional<Integer> appBuildNumber){
            app = app.appBuildNumber(appBuildNumber);
            return this;
        }

        @Override
        public Builder cost(Optional<Cost> cost)
        {
            this.cost = checkNotNull(cost);
            return this;
        }

        public Builder hosting(Optional<HostingType> hosting)
        {
            this.hosting = checkNotNull(hosting);
            return this;
        }

        @Override
        public Builder includePrivate(boolean includePrivate)
        {
            this.includePrivate = includePrivate;
            return this;
        }
        
        /**
         * Specifies an existing version if you want to query versions that are later than that
         * version.
         * @param versionName  the version string (for instance, "1.0") of an existing add-on version
         *   to restrict the query to versions later than this version, or {@link Optional#empty()} to
         *   remove any such restriction 
         * @return  the same query builder
         * @see AddonVersionsQuery#safeGetAfterVersionName()
         */
        public Builder afterVersion(Optional<String> versionName)
        {
            this.afterVersionName = checkNotNull(versionName);
            return this;
        }

        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
    }
}
