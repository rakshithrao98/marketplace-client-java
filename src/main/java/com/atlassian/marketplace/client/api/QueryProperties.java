package com.atlassian.marketplace.client.api;

import com.atlassian.marketplace.client.util.Convert;
import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import io.atlassian.fugue.Option;

import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Iterables.concat;
import static io.atlassian.fugue.Option.none;
import static io.atlassian.fugue.Option.some;

/**
 * Interfaces for common query criteria, allowing classes like {@link AddonQuery} to be treated abstractly.
 * @since 2.0.0
 */
public abstract class QueryProperties
{
    private QueryProperties()
    {
    }
    
    /**
     * Interface for query criteria that include an "accessToken" parameter.
     * @see QueryBuilderProperties.AccessToken
     */
    public interface AccessToken
    {
        /**
         * The access token, if any, that the client is providing for the query (in order to allow
         * access to a private listing).
         * @see QueryBuilderProperties.AccessToken#accessToken(Optional)
         */
        Optional<String> safeGetAccessToken();
    }
    
    /**
     * Interface for query criteria that include "application" and "application build number" parameters.
     * @see QueryBuilderProperties.ApplicationCriteria
     */
    public interface ApplicationCriteria
    {
        /**
         * The key of the application, if any, that the client is using to restrict the query results.
         * @see QueryBuilderProperties.ApplicationCriteria#application(Optional)
         */
        Optional<ApplicationKey> safeGetApplication();

        /**
         * The application build number, if any, that the client is using to restrict the query results.
         * @see QueryBuilderProperties.ApplicationCriteria#appBuildNumber(Optional)
         */
        Optional<Integer> safeGetAppBuildNumber();
    }
    
    /**
     * Interface for query criteria that include "offset" and "limit" parameters.
     * @see QueryBuilderProperties.Bounds
     */
    public interface Bounds
    {
        /**
         * The offset/limit parameters attached to this query.
         * @see QueryBuilderProperties.Bounds#bounds(QueryBounds)
         */
        QueryBounds getBounds();
    }

    /**
     * Interface for query criteria that include a "cost" parameter.
     * @see QueryBuilderProperties.Cost
     */
    public interface Cost
    {
        /**
         * The {@link Cost} filter, if any, that the client is using to restrict the query results.
         * @see QueryBuilderProperties.Cost#cost(Optional)
         */
        Optional<com.atlassian.marketplace.client.api.Cost> safeGetCost();
    }

    /**
     * Interface for query criteria that include a "hosting" parameter.
     * @see QueryBuilderProperties.Hosting
     */
    public interface Hosting
    {
        /**
         * The hosting type that the client is using to restrict the query, if any.
         * @see QueryBuilderProperties.Hosting#hosting(Optional)
         */
        Optional<HostingType> safeGetHosting();
    }

    /**
     * Interface for query criteria that include a multi-valued "hosting" parameter.
     * @see QueryBuilderProperties.MultiHosting
     * @since 2.1.0
     */
    public interface MultiHosting
    {
        /**
         * The hosting type that the client is using to restrict the query, if any.
         * @see QueryBuilderProperties.MultiHosting#hosting(List)
         */
        List<HostingType> getHostings();

    }

    /**
     * Interface for query criteria that include a "includePrivate" parameter.
     * @see QueryBuilderProperties.IncludePrivate
     */
    public interface IncludePrivate
    {
        /**
         * The "include private" flag that the client is using to restrict the query, if any.
         * @see QueryBuilderProperties.IncludePrivate#includePrivate(boolean)
         */
        boolean isIncludePrivate();
    }

    /**
     * Interface for query criteria that include a "withVersion" parameter.
     * @see QueryBuilderProperties.WithVersion
     */
    public interface WithVersion
    {
        /**
         * True if the client is requesting version-level detail in the result set.
         * @see QueryBuilderProperties.WithVersion#withVersion(boolean)
         */
        boolean isWithVersion();
    }
    
    // helper methods used internally
    
    static String describeParams(String className, Iterable<String>... paramLists)
    {
        return className + "(" + Joiner.on(", ").join(concat(paramLists)) + ")";
    }
    
    static Iterable<String> describeOptBoolean(String name, boolean value)
    {
        return Convert.iterableOf(value ? Optional.of(name + "(true)") : Optional.empty());
    }
    
    static <T extends EnumWithKey> Iterable<String> describeOptEnum(String name, Option<T> value)
    {
        for (T v: value)
        {
            return some(name + "(" + v.getKey() + ")");
        }
        return none();
    }

    static <T extends EnumWithKey> Iterable<String> describeOptEnum(String name, Iterable<T> value)
    {
        for (T v: value)
        {
            return some(name + "(" + v.getKey() + ")");
        }
        return none();
    }

    static Iterable<String> describeValues(String name, Iterable<?> values)
    {
        if (!Iterables.isEmpty(values))
        {
            return some(name + "(" + Joiner.on(",").join(values) + ")");
        }
        return none();
    }
}
