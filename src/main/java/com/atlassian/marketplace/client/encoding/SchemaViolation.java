package com.atlassian.marketplace.client.encoding;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Base class for error descriptors that indicate that a response from the Marketplace server did
 * not conform to the expected JSON schema, or that a model object constructed by the caller (to
 * be sent to the server) was invalid.
 * @since 2.0.0
 */
public abstract class SchemaViolation
{
    private final Class<?> schemaClass;
    
    protected SchemaViolation(Class<?> schemaClass)
    {
        this.schemaClass = checkNotNull(schemaClass);
    }
    
    /**
     * The class of the model object that was involved in the error.
     */
    public Class<?> getSchemaClass()
    {
        return schemaClass;
    }
    
    /**
     * A human-readable string describing the error.
     */
    public abstract String getMessage();
    
    @Override
    public String toString()
    {
        return getMessage();
    }
}
