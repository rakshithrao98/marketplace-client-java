package com.atlassian.marketplace.client.encoding;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Indicates that an object returned by the server (or constructed by the caller) was invalid
 * because a required link relation was omitted.
 * @since 2.0.0
 */
public class MissingRequiredLink extends SchemaViolation
{
    private final String rel;
    
    public MissingRequiredLink(Class<?> schemaClass, String rel)
    {
        super(schemaClass);
        this.rel = checkNotNull(rel);
    }
    
    /**
     * The link relation string of the link that was not found.
     */
    public String getRel()
    {
        return rel;
    }
    
    @Override
    public String getMessage()
    {
        return "missing required link \"" + rel + "\" in " + getSchemaClass().getSimpleName();
    }
}
