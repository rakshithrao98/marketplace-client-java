#!/bin/bash
git config --global user.email "$GIT_USER_EMAIL"
git config --global user.name "$GIT_USER_NAME"
mkdir -p ~/.ssh
(umask  077 ; echo $MPCJ_DOCS_SSH_PRIVATE | base64 --decode > ~/.ssh/mpcj_docs_rsa)
eval `ssh-agent -s`
ssh-add ~/.ssh/mpcj_docs_rsa